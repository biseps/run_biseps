
# quick script to check how many lines
# are in each BiSEPS output file.

# Used to verify that any source-code changes to BiSEPS
# haven't impacted on the output totals.

# Note:
# only bother to check five folders (10, 11, 12, 13, 14, 15) 
# inside the "biseps_b" folder

wc -l ./sol/biseps_s/1/*dat*
wc -l ./sol/biseps_b/{10..15}/*dat* 

printf "\n\n\n"

wc -l ./subSol/biseps_s/1/*dat*
wc -l ./subSol/biseps_b/{10..15}/*dat*




# Compare against these standard totals from:
    # /padata/beta/users/efarrell/data/biseps2_runs/07_run/


# sol
# ---

# biseps_s

# 328208 ./WEBwide_pop.dat.1
# 328208 ./WEBwide_mag.dat.1
# 328208 ./WEBwide_kepler.dat.1


# biseps_b

# 18258 10/WEB_kepler.dat.1
# 18258 10/WEB_mag.dat.1
# 18258 10/WEB_pop.dat.1

# 16018 11/WEB_kepler.dat.1
# 16018 11/WEB_mag.dat.1
# 16018 11/WEB_pop.dat.1

# 16346 12/WEB_kepler.dat.1
# 16346 12/WEB_mag.dat.1
# 16346 12/WEB_pop.dat.1

# 17828 13/WEB_kepler.dat.1
# 17828 13/WEB_mag.dat.1
# 17828 13/WEB_pop.dat.1

# 19113 14/WEB_kepler.dat.1
# 19113 14/WEB_mag.dat.1
# 19113 14/WEB_pop.dat.1

# 23155 15/WEB_kepler.dat.1
# 23155 15/WEB_mag.dat.1
# 23155 15/WEB_pop.dat.1




# subSol
# ------

# biseps_s

# 318465 ./WEBwide_mag.dat.1
# 318465 ./WEBwide_pop.dat.1
# 318465 ./WEBwide_kepler.dat.1



# biseps_b

# 27758 10/WEB_kepler.dat.1
# 27758 10/WEB_mag.dat.1
# 27758 10/WEB_pop.dat.1

# 26742 11/WEB_kepler.dat.1
# 26742 11/WEB_mag.dat.1
# 26742 11/WEB_pop.dat.1

# 27562 12/WEB_kepler.dat.1
# 27562 12/WEB_mag.dat.1
# 27562 12/WEB_pop.dat.1

# 30380 13/WEB_kepler.dat.1
# 30380 13/WEB_mag.dat.1
# 30380 13/WEB_pop.dat.1

# 36370 14/WEB_kepler.dat.1
# 36370 14/WEB_mag.dat.1
# 36370 14/WEB_pop.dat.1

# 40870 15/WEB_kepler.dat.1
# 40870 15/WEB_mag.dat.1
# 40870 15/WEB_pop.dat.1





