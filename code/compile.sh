#!/bin/bash


# location of source-code files
code_folder="/padata/beta/users/pmr257/repos/biseps"


make_file="$code_folder/pop/Makefile_BiSEPS"
make_vpath="$code_folder/pop"


printf "\nUsing source code from: $code_folder\n\n"


# Compile BiSEPS

printf "\nremoving existing BiSEPS...\n\n"
make -f $make_file folder=$make_vpath clobber

printf "\ncompiling BiSEPS...\n\n"
make -f $make_file folder=$make_vpath 



if [ -f "BiSEPS" ]; then
    # compile successful
    printf "\n\nBiSEPS is now ready to run.\n"
    printf "Goto folder '../cluster' and run 'jobSubmit.sh'\n\n"
fi



