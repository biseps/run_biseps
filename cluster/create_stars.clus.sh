#!/bin/bash

# Purpose:
#   Run 1 single cluster job which creates evolutionary tracks
#   for SINGLE STARS with different masses...


#$ -N do_stars
#$ -l s_rt=200:00:00,virtual_free=3G,h_vmem=6G
#$ -t 1-1:1
#$ -S /bin/bash
#$ -j y
#$ -cwd

# email me when job:
# (b)egins 
# (e)nds 
# (a)borts 
# (s)uspends

#$ -M pam.rowden27@gmail.com
#$ -m as


main() 
{
    # get utility functions
    source "$SOURCE_CODE_DIR/common/utility_functions.sh"


    # fortran/python programs used in this script
    program_biseps="$COMPILED_BISEPS_DIR/BiSEPS"
    program_mag="$SOURCE_CODE_DIR/mag/absMag.py"


    # setup environment
    validate_variables
    set_python_env  anaconda  "$SOURCE_CODE_DIR"


    # misc variables
    IS_SINGLE="1"
    FOLDER_SOLAR="$RUNPATH/sol/biseps_s/$SGE_TASK_ID"
    FOLDER_SUB_SOLAR="$RUNPATH/subSol/biseps_s/$SGE_TASK_ID"


    # single star population - solar metallicity 
    cd $FOLDER_SOLAR
    create_single_stars


    # single star population - sub-solar metallicity 
    cd $FOLDER_SUB_SOLAR
    create_single_stars
}



create_single_stars() 
{
    $program_biseps BiSEPS.in.1
    $PYTHONBIN $program_mag WEBwide_kepler.dat.1 $IS_SINGLE > WEBwide_mag.dat.1
}



validate_variables()
{

    # make sure directories exist
    assert_folder_exists  "RUNPATH"                 "$RUNPATH" 
    assert_folder_exists  "SOURCE_CODE_DIR"         "$SOURCE_CODE_DIR" 
    assert_folder_exists  "COMPILED_BISEPS_DIR"     "$COMPILED_BISEPS_DIR" 


    # make sure files exist
    assert_file_exists    "program_biseps"          "$program_biseps"      
    assert_file_exists    "program_mag"             "$program_mag"           
}




# Execute main function
main

