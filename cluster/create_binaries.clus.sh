#!/bin/bash

# Purpose:
#   Run 150 cluster jobs which create evolutionary tracks
#   for binary systems with different masses, orbital periods etc


#$ -N do_binary
#$ -l s_rt=200:00:00,virtual_free=3G,h_vmem=6G,h_fsize=2G
#$ -t 1-150:1
#$ -S /bin/bash
#$ -j y
#$ -cwd

# email me when job: 
# (a)borts, 
# (s)uspends

#$ -M pam.rowden27@gmail.com
#$ -m as


main() 
{

    # get utility functions
    source "$SOURCE_CODE_DIR/common/utility_functions.sh"


    # fortran/python programs used in this script
    program_biseps="$COMPILED_BISEPS_DIR/BiSEPS"
    program_mag="$SOURCE_CODE_DIR/mag/absMag.py"


    # setup environment
    validate_variables
    set_python_env  anaconda  "$SOURCE_CODE_DIR"


    # misc variables
    IS_BINARY="0"
    FOLDER_SOLAR="$RUNPATH/sol/biseps_b/$SGE_TASK_ID"
    FOLDER_SUB_SOLAR="$RUNPATH/subSol/biseps_b/$SGE_TASK_ID"


    # Create binary population - 
    # with solar metallicity 
    cd $FOLDER_SOLAR
    create_binaries


    # Create binary population - 
    # with sub-solar metallicity 
    cd $FOLDER_SUB_SOLAR
    create_binaries
}



create_binaries() 
{
    $program_biseps BiSEPS.in.1
    $PYTHONBIN $program_mag WEB_kepler.dat.1 $IS_BINARY > WEB_mag.dat.1
}



validate_variables()
{

    # make sure directories exist
    assert_folder_exists  "RUNPATH"                 "$RUNPATH" 
    assert_folder_exists  "SOURCE_CODE_DIR"         "$SOURCE_CODE_DIR" 
    assert_folder_exists  "COMPILED_BISEPS_DIR"     "$COMPILED_BISEPS_DIR" 


    # make sure files exist
    assert_file_exists    "program_biseps"      "$program_biseps"      
    assert_file_exists    "program_mag"         "$program_mag"           
}



# Launch main function
main



