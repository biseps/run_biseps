#!/bin/bash

#$ -N do_merge
#$ -l s_rt=200:00:00,virtual_free=3G,h_vmem=6G
#$ -S /bin/bash
#$ -j y
#$ -cwd

# email me when job:
# (b)egins 
# (e)nds 
# (a)borts 
# (s)uspends

#$ -M pam.rowden27@gmail.com
#$ -m as


# get utility functions
source "$SOURCE_CODE_DIR/common/utility_functions.sh"


# fortran/python programs used in this script
merge_program="$SOURCE_CODE_DIR/scripts/mergeBiSEPS.py"


# setup environment
set_python_env  anaconda  "$SOURCE_CODE_DIR"


# make sure folders/files exist
assert_folder_exists  "SOURCE_CODE_DIR"     "$SOURCE_CODE_DIR" 
assert_file_exists    "merge_program"       "$merge_program"      



# loop through each directory
# inside 'sol' and 'subSol'
for i in sol subSol;
do

	cd "$RUNPATH"/$i

    # merge binary systems
	$PYTHONBIN $merge_program   WEB_pop.dat.1         biseps_b/ 150
	$PYTHONBIN $merge_program   WEB_kepler.dat.1      biseps_b/ 150
	$PYTHONBIN $merge_program   WEB_mag.dat.1         biseps_b/ 150

    # merge single stars
	$PYTHONBIN $merge_program   WEBwide_pop.dat.1     biseps_s/ 1
	$PYTHONBIN $merge_program   WEBwide_kepler.dat.1  biseps_s/ 1
	$PYTHONBIN $merge_program   WEBwide_mag.dat.1     biseps_s/ 1

	cp biseps_b/1/WEB_grid.dat.1 .
	cp biseps_s/1/WEBwide_grid.dat.1 .

done
